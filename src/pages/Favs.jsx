import React, { useEffect, useState } from "react";
import { useAppContext } from "../context/AppContext";
import Card from "../components/Card/Card";
import { Link } from "react-router-dom";

const Favs = () => {
  const { state } = useAppContext();

  const [uniqueFavs, setUniqueFavs] = useState(state.favorites);

  useEffect(() => {
    setUniqueFavs(state.favorites);
  }, [state.favorites]);

  return (
    <div>
      <h1>Favoritos</h1>
      {(!uniqueFavs || uniqueFavs.length === 0) ? (
        <p className="Detail NotFound">You don't have favorite dentists</p>
      ) : (
        <div className="card-grid">
          {uniqueFavs.map((fav) => (
            <Card dentist={fav} />
          ))}
        </div>
      )}
    </div>
  );
};

export default Favs;
