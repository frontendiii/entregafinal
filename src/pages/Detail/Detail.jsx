import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { useAppContext } from '../../context/AppContext'
import './Detail.css'

const Detail = () => {
  const { id } = useParams()
  const { state, fetchDataDetail } = useAppContext()
  
  useEffect(() => {
    if (id) {
      fetchDataDetail(id);
    }
  }, [id]);

  const dentist = state.dentist;

  if (!dentist || Object.keys(dentist).length === 0) {
    return <p className='Detail NotFound'>Dentist Not Found</p>;
  }

  return (
    <div className='Detail'>
      <h1>Detalle Dentista {dentist.id}</h1>
      <p>Name: {dentist.name}</p>
      <p>Email: {dentist.email}</p>
      <p>Phone: {dentist.phone}</p>
      <p>Website: {dentist.website}</p>
    </div>
  )
}

export default Detail