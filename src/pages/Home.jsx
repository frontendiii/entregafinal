import React from 'react'
import { useAppContext } from '../context/AppContext'
import Card from '../components/Card/Card'

const Home = () => {
  const { state } = useAppContext()

  return (
    <main>
      <h1>Home</h1>
      <div className='card-grid'>
        {state.dentists.map((dentist) => (
          <Card key={dentist.id} dentist={dentist} />
        ))}
      </div>
    </main>
  )
}

export default Home