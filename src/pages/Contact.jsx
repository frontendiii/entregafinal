import React from 'react'
import Form from '../components/Form/Form'

const Contact = () => {
  return (
    <div className='Contact'>
      <h2>Want to know more?</h2>
      <p>Send us your questions and we will contact you</p>
      <Form />
    </div>
  )
}

export default Contact