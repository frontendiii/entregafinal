import React from 'react'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import Navbar from './components/Navbar/Navbar'
import Footer from './components/Footer/Footer'
import Home from './pages/Home'
import Favs from './pages/Favs'
import Contact from './pages/Contact'
import Detail from './pages/Detail/Detail'
import { useAppContext } from './context/AppContext'

import './App.css'

function App() {
  const { state } = useAppContext()
  return (
    <Router>
      <div className={`${state.theme}`}>
        <Navbar />
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='/home' element={<Home />} />
          <Route path='/favs' element={<Favs />} />
          <Route path='/contacto' element={<Contact />} />
          <Route path='/detalle/:id' element={<Detail />} />
          <Route path='*' element={() => <h1>Not Found</h1>} />
        </Routes>
        <Footer />
      </div>
    </Router>
  )
}

export default App
