import React from 'react';
import './Navbar.css';
import Sun from '../../assets/Sun.svg';
import Moon from '../../assets/Moon.svg';
import { Link } from 'react-router-dom';
import { useAppContext } from '../../context/AppContext';
import { TOGGLE_THEME } from '../../hooks/useReducerHook';

const Navbar = () => {
  const { state, dispatch } = useAppContext();

  const routes = [
    { path: '/home', name: 'Home' },
    { path: '/favs', name: 'Favs' },
    { path: '/contacto', name: 'Contact' },
  ]

  const handleTheme = () => {
    dispatch({ type: TOGGLE_THEME });
  }
  return (
    <nav className='navbar'>
      <ul>
        {routes.map((route, index) => (
          <li key={index}>
            <Link to={route.path}>{route.name}</Link>
          </li>
        ))}
      </ul>
      <button className='btn-theme' onClick={handleTheme}><img src={state.theme === 'light' ? Moon : Sun}></img></button>
    </nav>
  );
};

export default Navbar;