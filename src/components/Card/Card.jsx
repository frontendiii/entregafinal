import React from 'react'
import './Card.css'
import Info from '../../assets/Info.svg'
import DoctorImage from '../../assets/doctor.jpg'
import { Link } from 'react-router-dom'
import ButtonFav from '../ButtonFav/ButtonFav'

const Card = ({ dentist }) => {

  return (
    <div className="card">
        <div className="card-header">
          <Link className="circle-button info" to={`/detalle/${dentist.id}`}>
            <img src={Info}></img>
          </Link>
          <ButtonFav dentist={dentist}/>
        </div>
        <div className="card-body">
          <img src={DoctorImage} alt="Doctor"/>
          <h5 className="card-title-sm">{dentist.name}</h5>
          <h6 className="card-label-sm">{dentist.username}</h6>
        </div>
        <p className="id">{dentist.id}</p>
    </div>
  )
}

export default Card