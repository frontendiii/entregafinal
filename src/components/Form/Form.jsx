import React, { useState } from "react";
import { set, useForm } from "react-hook-form";
import "./Form.css";

const Form = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    setValue,
    reset,
  } = useForm();

  const [form, setForm] = useState(false);
  const [fullName, setFullName] = useState("");

  const onSubmit = (data) => {
    setForm(true);
    setFullName(data.fullName);
  };

  const resetForm = () => {
    setForm(false);
    setFullName("");
    reset();
  }

  return (
    <div className="Form">
      {form ? (
        <div>
          <p>{`Gracias ${fullName}, te contactaremos cuanto antes vía email`}</p>
          <div className="container-button">
            <button onClick={resetForm}>Volver al formulario</button>
          </div>
        </div>
      ) : (
        <form onSubmit={handleSubmit(onSubmit)}>
          <label>
            Nombre completo:
            <input
              {...register("fullName", {
                required: "Campo requerido",
                minLength: {
                  value: 5,
                  message: "Por favor verifique su información nuevamente",
                },
              })}
              onChange={(e) => setValue("fullName", e.target.value)}
            />
          </label>
          {errors.fullName && <p>{errors.fullName.message}</p>}

          <label>
            Email:
            <input
              {...register("email", {
                required: "Campo requerido",
                pattern: {
                  value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                  message: "Por favor verifique su información nuevamente",
                },
              })}
            />
          </label>
          {errors.email?.message && <p>{errors.email.message}</p>}
          <div className="container-button">
            <button type="submit">Enviar</button>
          </div>
        </form>
      )}
    </div>
  );
};

export default Form;
